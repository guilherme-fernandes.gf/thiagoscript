#!/bin/bash

echo "Criando o diretório /tmp/DATA"

data=$(date +"%Y-%m-%d-%H")

mkdir "/tmp/$data"

cp ./* /tmp/$data 2> /dev/null 

echo "Tudo certo"
