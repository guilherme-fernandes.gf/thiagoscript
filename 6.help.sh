#!/bin/bash

echo "Substituição de variáveis:"

nome=" João "
echo "Olá, ${nome}"


echo "Substituição de shell:"
echo "Você consegue guardar um comando executado dentro de uma variável"

arquivos=$(ls)
echo "Os arquivos do diretório atual são ${arquivos}"


echo "Substituição Arimética:"
echo " Uma expressão aritmética entre parênteses duplos precedida por um sinal de dolar ("$(())") é substituída pelo valor da expressão arimética entre parênteses duplos."

num1=6
num2=2
soma=$((num1 + num2))
echo "A soma de ${num1} e ${num2} é ${soma}"





