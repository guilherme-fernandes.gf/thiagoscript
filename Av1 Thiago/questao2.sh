#/bin/bash

echo -e "Digite o nome do primeiro diretório:"
read d1

echo -e "Digite o nome do segundo diretório:"
read d2

echo -e "Digite o nome do terceiro diretório:"
read d3

txt=0
png=0
doc=0

for diretorio in "$d1" "$d2" "$d3";do
	if [ -d "$diretorio" ];then
		for arq in "$diretorio";do
			if [ "{$arq}" = "txt" ];then
				((txt++))
			elif [ "{$arq}" = "png" ];then
				((png++))
			elif [ "{$arq}" = "doc" ];then
				((doc++))
			fi
		done
			else 
				echo "O diretório "$diretorio" não foi encontrado ou não existe"
			fi
		done


echo "Quantidade de arquivos txt:" "$txt"
echo "Quantidade de arquivos png:" "$png"
echo "Quantidade de arquivos doc:" "$doc"
