#/bin/bash

echo "Receber 3 nomes de arquivo como parâmetros de linha de comando"

nome1=$1
nome2=$2
nome3=$3

linhaarq1=$(wc -l <${nome1})
linhaarq2=$(wc -l <${nome2})
linhaarq3=$(wc -l <${nome3})

resultado=$((linhaarq1 + linhaarq2 + linhaarq3))

echo "A soma dos número de linhas é: ${resultado}"

