	#/bin/bash

echo -e " Faça sempre o seu melhor!"
echo -e " A felicidade é feita pelas suas próprias ações"
echo -e " A receita para o sucesso está no equilíbrio"
echo -e " O segredo do sucesso é a moderação, ter o dia sim e ter o dia não"
echo -e " O sábio é aquele que conhece os limites da própria ignorância"
echo -e " Os problemas são oportunidades para se mostrar o que sabe"
echo -e " Desafios nos tornam mais fortes e resilientes, não desista"
echo -e " Nossos fracassos, às vezes, são mais frutíferos do que os êxitos"
echo -e " Tente de novo. Fracasse de novo. Mas fracasse melhor"
echo -e " Eu faço da dificuldade a minha motivcação. A volta por cima vem na continuação"
