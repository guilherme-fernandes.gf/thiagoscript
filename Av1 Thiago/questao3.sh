#/bin/bash

echo "Maneira de criar variável com valor fixo"

a="2"

echo "Criando várivel com entrada para o usuário"
echo "Digite um valor:"
read b

echo "Variável na linha de comando"
c=$1

echo "Variável a partir da resposta de um comando"
z=$(pwd)

echo "Exibindo os valores das variáveis de cada maneira de ser criada:"

echo "A = $a"
echo "B = $b"
echo "C = $c"
echo "Z = $z"


echo "Quando a variável é criado com entrada para o usuário, o script irá solicitar o valor e armazenar o valor na variável criada."

echo "Quando a variável é criada na linha de comando, recebemos um valor como um parâmetro na linha de comando e o valor é colocado na variável diretamente."
