#!/bin/bash

data_atual=$(date +"%d-%m-%Y")

datasemananum=$(date -d "data_atual" +%u)

dapq=$(( (3 - datasemananum + 7) % 7 ))

prox_quarta=$(date -d "$data_atual + dapq" + "%d-%m-%Y)

echo "Próxima quarta-feira será $prox_quarta"

backup-data="backup($prox_quarta)"
mkdir "$backup-data"

