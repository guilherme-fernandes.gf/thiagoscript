#!/bin/bash

read -p "Digite as seguinte operações (+,-,*,/, sqrt): " op
read -p "Primeiro número: " num1
read -p "Segundo número: " num2

result=0

test "$op" = "+" && result=$(echo "$num1 + $num2" | bc)

test "$op" = "-" && result=$(echo "$num1 - $num2" | bc)

test "$op" = "*" && result=$(echo "$num1 * $num2" | bc)

test "$op" = "/" && result=$(echo "$num1 / $num2" | bc)

test "$op" = "sqrt" && result=$(echo "sqrt($num1)" | bc)

echo "Resultado $result"
