#!/bin/bash

echo "Se deseja voltar para o prompt original, pressione 5"
ps1_backup="$1"

echo "Escolha uma das opções para personalização do seu prompt :D : "

echo "1- Prompt Verde"
echo "2- Prompt amarelo"
echo "3- Prompt azul"
echo "4- Prompt personalizado com informações extras. Ex: caminho do diretório, hostname"
echo "5- Voltar para o prompt original"

read -p "Opção: " op

[ "$op" = "1" ] && PS1='\[\033[0;32m\]\u@\h: \w\$ \[\033[0m\]'
[ "$op" = "2" ] && PS1='\[\033[0;33m\]\u@\h: \w\$ \[\033[0m\]'
[ "$op" = "3" ] && PS1='\[\033[0;34m\]\u@\h: \w\$ \[\033[0m\]'
[ "$op" = "4" ] && PS1='\u \w \h: '
[ "$op" = "5" ] && PS1='$ps1_backup'

