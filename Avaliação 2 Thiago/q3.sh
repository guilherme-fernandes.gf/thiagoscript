#!/bin/bash

nome="$1"

test -e "$nome" && echo "Arquivo "$nome" está localizado no diretório atual $(pwd)" && exit

test -e "/tmp/$nome" && echo "Arquivo "$nome" localizado no diretório /tmp" &&
 exit

test -e "/etc/$nome" && echo "Arquivo "$nome" localizado no diretório /etc" && exit

echo "Error, arquivo "$nome" não encontrado em nenhum dos diretórios citados"
