#!/bin/bash

num1=$1
num2=$2

soma=$((num1 + num2))

echo "A soma de ${num1} e ${num2} é igual a ${soma}"
