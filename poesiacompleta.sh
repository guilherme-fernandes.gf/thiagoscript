#!/bin/bash

echo -e "\e[1;34m Soneto do Amor Total \e[0m"
sleep 1
echo -e "\e[1;30m Amo-te tanto, meu amor... não cante \e[0m"
sleep 1
echo -e "\e[1;32m O humano coração com mais verdade... \e[0m"
sleep 1
echo -e "\e[1;33m Amo-te como amigo e como amante \e[0m"
sleep 1
echo -e "\e[1;34m Numa sempre diversa realidade \e[0m"
sleep 1
echo -e "\e[1;34m Amo-te afim, de um calmo amor prestante, \e[0m"
sleep 1
echo -e "\e[1;33m E te amo além, presente na saudade. \e[0m"
sleep 1
echo -e "\e[1;37m Amo-te, enfim, com grande liberdade \e[0m"
sleep 1
echo -e "\e[1;32m Dentro da eternidade e a cada instante \e[0m"
sleep 1
echo -e "\e[1;30m Amo-te como um bicho, simplesmente, \e[0m"
sleep 1
echo -e "\e[1;34m De um amor sem mistério e sem virtude \e[0m"
sleep 1
echo -e "\e[1;37m Com um desejo maciço e permanente \e[0m"
sleep 1
echo -e "\e[1;34m E de te amar assim muito e amiúde \e[0m"
sleep 1
echo -e "\e[1;35m É que um dia em teu corpo de repente \e[0m"
sleep 1
echo -e "\e[1;34m Hei de morrer de amar mais do que pude \e[0m"
sleep 1

echo -e "\e[1;35m Vinicius de Moraes \e[0m"
