#!/bin/bash


sucesso=0
falhas=0

sites=("www.google.com" "www.facebook.com" "www.youtube.com" "www.stackoverflow.com")

ping -c 1 "${sites[0]}" >/dev/null 2>&1 && ((sucesso++)) || ((falha++))
ping -c 1 "${sites[1]}" >/dev/null 2>&1 && ((sucesso++)) || ((falha++))
ping -c 1 "${sites[2]}" >/dev/null 2>&1 && ((sucesso++)) || ((falha++))
ping -c 1 "${sites[3]}" >/dev/null 2>&1 && ((sucesso++)) || ((falha++))

echo "Pings sucess: $sucesso"
echo "Pings fails: $falhas"
