#!/bin/bash

[ ! -f "$1" ] && echo "O arquivo $1 não existe." && exit 1


echo "Menu:"
echo "1. Exibir as 10 primeiras linhas do arquivo"
echo "2. Exibir as 10 últimas linhas do arquivo"
echo "3. Exibir o número de linhas total do arquivo"

read -p "Digite o respectivo número da opção desejada: " op

[ "$op" -eq 1 ] && head -n 10 "$1"
[ "$op" -eq 2 ] && tail -n 10 "$1"
[ "$op" -eq 3 ] && wc -l < "$1"




