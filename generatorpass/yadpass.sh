#!/bin/bash

# Exibe a interface gráfica para obter as opções do usuário
yad_result=$(yad --form --title "Gerador de Senhas" \
        --field="Tamanho da senha:NUM" \
        --field="Letras maiúsculas:CHK" \
        --field="Letras minúsculas:CHK" \
        --field="Números:CHK" \
        --field="Caracteres especiais:CHK" \
        --button="Gerar Senhas":0 \
        --button="Cancelar":1)

# Verifica se o usuário clicou em "Cancelar"
if [ "$?" -eq 1 ]; then
    exit 0
fi

# Extrai as opções do usuário do resultado do Yad
password_length=$(echo "$yad_result" | awk -F '|' '{print $1}')
use_uppercase=$(echo "$yad_result" | awk -F '|' '{print $2}')
use_lowercase=$(echo "$yad_result" | awk -F '|' '{print $3}')
use_numbers=$(echo "$yad_result" | awk -F '|' '{print $4}')
use_special_chars=$(echo "$yad_result" | awk -F '|' '{print $5}')

# Verifica se pelo menos uma opção foi selecionada
if [ "$use_uppercase" = "FALSE" ] && [ "$use_lowercase" = "FALSE" ] && [ "$use_numbers" = "FALSE" ] && [ "$use_special_chars" = "FALSE" ]; then
    yad --error --text "Selecione pelo menos uma opção de caracteres."
    exit 1
fi

# Define os conjuntos de caracteres com base nas opções selecionadas
charsets=()
if [ "$use_uppercase" = "TRUE" ]; then
    charsets+=("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
fi
if [ "$use_lowercase" = "TRUE" ]; then
    charsets+=("abcdefghijklmnopqrstuvwxyz")
fi
if [ "$use_numbers" = "TRUE" ]; then
    charsets+=("0123456789")
fi
if [ "$use_special_chars" = "TRUE" ]; then
    charsets+=("!@#$%^&*()")
fi

# Concatena todos os conjuntos de caracteres em uma única string
all_chars=$(printf '%s' "${charsets[@]}")

# Gera três senhas aleatórias
generated_passwords=()
for i in {1..3}; do
    password=$(echo "$all_chars" | fold -w1 | shuf | head -n$password_length | tr -d '\n')
    generated_passwords+=("$password")
done

# Exibe as senhas geradas na tela
yad --info --text "Senhas geradas:
${generated_passwords[0]}
${generated_passwords[1]}
${generated_passwords[2]}"


# Salva as senhas geradas em um arquivo
echo "${generated_passwords[@]}" > passgenerated.txt

# Exibe mensagem de sucesso
yad --info --text "As senhas geradas foram salvas no arquivo passgenerated.txt."
