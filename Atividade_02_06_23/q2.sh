#!/bin/bash

[ $# -ne 1 ] && { echo "Digite um número da linha de comando." ; exit 1; }

n=$1
soma=0

for (( i=1; i<=n; i++ )); do
	soma=$(($soma + i))	
done

echo "A soma de todos os números de 1 até $1 é: $soma "
