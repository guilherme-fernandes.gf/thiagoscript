#!/bin/bash


arq() {
	tipo_arq=""
	[ -d "$1" ] && tipo_arq="Diretorio"
	[ -f "$1" ] && tipo_arq="Arquivo"
	[ -L "$1" ] && tipo_arq="Link simbolico"
	[ -x "$1" ] && tipo_arq="Executavel"

	echo "$tipo_arq: $1"
}

echo "Diretorio /tmp"
for file in /tmp/*; do
	tipo_arq=$(arq "$file")
	echo "$tipo_arq: $file"
done

echo "Diretório /etc"
for file in /etc/*; do
	tipo_arq=$(arq "$file")
	echo "$tipo_arq: $file"
done
