#!/bin/bash

while IFS= read -r arq; do
    hash=$(sha256sum "$arquivo" | cut -d ' ' -f 1)  
    echo "$arquivo $hash" >> resultado.txt
done < lista.txt
