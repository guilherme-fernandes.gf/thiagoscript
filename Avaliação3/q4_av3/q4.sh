#!/bin/bash

source funcoes_q4.sh


gerar_vida

nome_drag="Smaug" #não consegui pensar em outro dragão da cultura geek :(


while [[ $vida_player -gt 0 && $vida_drag -gt 0 ]]; do
    clear
    cowsay -f dragon "$nome_drag"
    exibir_menu
    read -p "Escolha uma opção: " op

    case $op in
        1)  
            atacar $nome_drag
            ;;
        2)  
            fugir $nome_drag
            ;;
        3) 
            curar $nome_drag
            ;;
        4)  
            exibir_info
            ;;
        *)  
            echo "Opção inválida! Tente novamente."
            ;;
    esac

    read -p "Pressione Enter para continuar..."
done

if [[ $vida_player -gt 0 ]]; then
    echo "Parabéns! Você derrotou o $nome_drag e venceu!!"
else
    echo "Você foi derrotado pelo $nome_drag. Não foi dessa vez!"
fi
