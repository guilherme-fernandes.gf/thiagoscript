#!/bin/bash

gerar_vida() {

    vida_player=$(( (RANDOM % 191) + 10 + 27 ))
    vida_drag=$(( (RANDOM % 4901) + 100 ))
}

exibir_menu() {
    echo "Opções:"
    echo "1 - Atacar"
    echo "2 - Fugir"
    echo "3 - Curar"
    echo "4 - Info"
}

atacar() {
    nome_drag=$1
    dano_player=$(( (RANDOM % 91) + 10 ))
    dano_drag_total=0

    echo "Você atacou o $nome_drag e causou $dano_player pontos de dano."

    for i in $(seq 1 $(( (RANDOM % 5) + 1 ))); do
        dano_dragao=$(( (RANDOM % 10) + 1 ))
        dano_drag_total=$((dano_drag_total + dano_drag))
        echo "O $nome_drag contra-atacou e causou $dano_drag pontos de dano."
    done

    vida_player=$((vida_player - dano_dragao_total))
}

fugir() {
    nome_drag=$1
    chance=$((RANDOM % 5))
    if [[ $chance -eq 0 ]]; then
        echo "Você conseguiu fugir do $nome_drag. Mas não passa nem wifi!!"
        vida_player=0
    else
        echo "Você tentou fugir, mas o $nome_drag te evaporou com suas chamas!!"
    fi
}

curar() {
	nome_drag=$1
	curar$(( (RANDOM % 51) + 50 ))
	chance_fuga_drag=$((RANDOM % 10))
	vida_player=$((vida_player + curar))

	echo "Você usou uma poção a tempo e curou $curar pontos de vida."
	if [[ $chance_fuga -eq 0 ]]; then
		echo "O $nome_drag tentou fugir de toda sua força, mas falhou"
	fi
} 

exibir_info() {
	echo "Sua vida: $vida_player
	echo "Vida do $nome_drag: $vida_drag"
}
