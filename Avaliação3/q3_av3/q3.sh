#!/bin/bash

vida_player=1
vida_drag=$2
nome_drag="Smaug" #referência ao Hobbit / senhor dos aneis hehe

while [[ $vida_player -gt 0 && $vida_drag -gt 0 ]]; do
    clear
    cowsay -f dragon "$nome_drag"
    echo "Opções:"
    echo "1 - Atacar"
    echo "2 - Fugir"
    echo "3 - Info"
    read -p "Escolha uma opção: " op

    case $op in
        1)  
            echo "Você atacou o $nome_drag e causou 100 pontos de dano."
            vida_drag=$((vida_drag - 100))
            if [[ $vida_drag -gt 0 ]]; then
                echo "O $nome_drag contra-atacou e causou 10 pontos de dano."
                vida_player=$((vida_player - 10))
            fi
            ;;
        2)  
            chance=$((RANDOM % 2))
            if [[ $chance -eq 0 ]]; then
                echo "Você conseguiu fugir do $nome_drag. Mas não passa nem wifi depois dessa!!"
                break
            else
                echo "Você tentou fugir, mas o $nome_drag te evaporou por completo com suas chamas!!"
                vida_player=0
            fi
            ;;
        3)  
            echo "Sua vida: $vida_player"
            echo "Vida do $nome_drag: $vida_drag"
            ;;
        *)  
            echo "Opção inválida! Tente novamente."
            ;;
    esac

    read -p "Pressione Enter para continuar..."
done

if [[ $vida_player -gt 0 ]]; then
    echo "Parabéns! Você derrotou o $nome_drag e venceu."
else
    echo "Você foi derrotado pelo $nome_drag. Naõ foi dessa vez!!"
fi
