#!/bin/bash

if [ $# -eq 0 ]; then
	echo "Forneça o nome de um usuário: "
	exit 1
fi

user=$1

while true; do
    clear
    echo "--- MENU ---"
    echo "1 - Verificar se o usuário existe"
    echo "2 - Verificar se o usuário está logado na máquina"
    echo "3 - Listar os arquivos da pasta home do usuário"
    echo "4 - Sair"

    read -p "Digite uma opção: " op

    case $op in
        1)
            id "$user" &>/dev/null
            if [ $? -eq 0 ]; then
                echo "O usuário $user existe"
            else
                echo "O usuário $user não existe"
            fi
            ;;

        2)
            who | grep -q "^$user "
            if [ $? -eq 0 ]; then
                echo "O usuário $user está logado na máquina"
            else
                echo "O usuário $user não está logado na máquina"
            fi
            ;;

        3)
	    id "$user" &>/dev/null
            if [ $? -eq 0 ]; then
                home_listar=$(getent passwd "$user" | cut -d ':' '-f 6')
                ls -l "$home_listar"
            else
                echo "O usuário $user não existe"
            fi
            ;;
        4)
            break
            ;;
        *)
            echo "Opção inválida, digite um dos códigos do menu"
            ;;
    esac

    read -p "Pressione Enter para continuar..."
done
