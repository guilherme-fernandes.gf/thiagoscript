#!/bin/bash

echo "Criando o diretório /tmp/DATA"

data=$(date +"%Y-%m-%d-%H")

mkdir "/tmp/$data"

cp ./* /tmp/$data 2> /dev/null

tar -czf "/tmp/$data.tar.gz" "/tmp/$data"

rm -r "/tmp/%data"

cp "/tmp/$data.tar.gz" .

echo "Tudo certo"
