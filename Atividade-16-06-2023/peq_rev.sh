#!/bin/bash

while true; do
    clear
    echo "--- MENU ---"
    echo "a -> Executar ping para um host (IP ou site)"
    echo "b -> Listar os usuários atualmente logados na máquina"
    echo "c -> Exibir o uso de memória e de disco da máquina"
    echo "d -> Sair"

    read -p "Digite uma opção: " opcao

    case $opcao in
        a)
            read -p "Digite o host (IP ou site): " host
            ping -c 4 $host
            ;;
        b)
            who
            ;;
        c)
            free -m
            df -h
            ;;
        d)
            break
            ;;
        *)
            echo "Opção inválida"
            ;;
    esac

    read -p "Pressione Enter para continuar..."
done
